<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>QObject</name>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/file-content-search-task.cpp" line="140"/>
        <source>Content index incomplete.</source>
        <translation>Index de contenu incomplet.</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/file-search-task.cpp" line="150"/>
        <source>Warning, Can not find home path.</source>
        <translation>Attention, impossible de trouver le chemin d’accueil.</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::AiSearchPlugin</name>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="44"/>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="220"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="45"/>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="221"/>
        <source>Open path</source>
        <translation>Chemin ouvert</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="46"/>
        <source>Copy Path</source>
        <translation>Copier le chemin</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="70"/>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="75"/>
        <source>AI Search</source>
        <translation>Recherche par l’IA</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="113"/>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="243"/>
        <source>OK</source>
        <translation>Oui</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="115"/>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="245"/>
        <source>Can not get a default application for opening %1.</source>
        <translation>Impossible d’obtenir une application par défaut pour l’ouverture de %1.</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="160"/>
        <source>File</source>
        <translation>Lime</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="198"/>
        <source>Path</source>
        <translation>Chemin</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="210"/>
        <source>Last time modified</source>
        <translation>Dernière modification</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="222"/>
        <source>Copy path</source>
        <translation>Copier le chemin d’accès</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::AppMatch</name>
    <message>
        <source>Application Description:</source>
        <translation type="vanished">应用描述：</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::AppSearch</name>
    <message>
        <source>Application Description:</source>
        <translation type="vanished">应用描述：</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::AppSearchPlugin</name>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="31"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="248"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="32"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="249"/>
        <source>Add Shortcut to Desktop</source>
        <translation>Ajouter un raccourci au bureau</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="33"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="250"/>
        <source>Add Shortcut to Panel</source>
        <translation>Ajouter un raccourci au panneau</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="34"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="251"/>
        <source>Install</source>
        <translation>Installer</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="64"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="69"/>
        <source>Applications Search</source>
        <translation>Recherche d’applications</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="154"/>
        <source>Application</source>
        <translation>Application</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="199"/>
        <source>Application Description:</source>
        <translation>Description de l’application :</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::AppSearchTask</name>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/app-search-task.cpp" line="55"/>
        <source>Application</source>
        <translation>Application</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/app-search-task.cpp" line="60"/>
        <source>Application search.</source>
        <translation>Recherche d’applications.</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::DirSearchPlugin</name>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="311"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="462"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="312"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="463"/>
        <source>Open path</source>
        <translation>Chemin ouvert</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="313"/>
        <source>Copy Path</source>
        <translation>Copier le chemin</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="332"/>
        <source>Dir Search</source>
        <translation>Recherche de répertoire</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="398"/>
        <source>Directory</source>
        <translation>Répertoire</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="327"/>
        <source>Dir search.</source>
        <translation>Recherche de répertoire.</translation>
    </message>
    <message>
        <source>directory</source>
        <translation type="vanished">目录</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="440"/>
        <source>Path</source>
        <translation>Chemin</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="452"/>
        <source>Last time modified</source>
        <translation>Dernière modification</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="464"/>
        <source>Copy path</source>
        <translation>Copier le chemin d’accès</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::FileContengSearchPlugin</name>
    <message>
        <source>Open</source>
        <translation type="vanished">Ouvrir</translation>
    </message>
    <message>
        <source>Open path</source>
        <translation type="vanished">Chemin ouvert</translation>
    </message>
    <message>
        <source>Copy Path</source>
        <translation type="vanished">Copier le chemin</translation>
    </message>
    <message>
        <source>File Content Search</source>
        <translation type="vanished">文本内容搜索</translation>
    </message>
    <message>
        <source>File content search.</source>
        <translation type="vanished">Recherche de contenu de fichier.</translation>
    </message>
    <message>
        <source>File content search</source>
        <translation type="vanished">Recherche dans le contenu d’un fichier</translation>
    </message>
    <message>
        <source>OCR</source>
        <translation type="vanished">OCR</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="vanished">Lime</translation>
    </message>
    <message>
        <source>Path</source>
        <translation type="vanished">Chemin</translation>
    </message>
    <message>
        <source>Last time modified</source>
        <translation type="vanished">Dernière modification</translation>
    </message>
    <message>
        <source>Copy path</source>
        <translation type="vanished">Copier le chemin d’accès</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::FileContentSearchPlugin</name>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="508"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="758"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="509"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="759"/>
        <source>Open path</source>
        <translation>Chemin ouvert</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="510"/>
        <source>Copy Path</source>
        <translation>Copier le chemin</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="533"/>
        <source>File content search.</source>
        <translation>Recherche de contenu de fichier.</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="538"/>
        <source>File content search</source>
        <translation>Recherche dans le contenu d’un fichier</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="622"/>
        <source>OCR</source>
        <translation>OCR</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="626"/>
        <source>File</source>
        <translation>Lime</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="736"/>
        <source>Path</source>
        <translation>Chemin</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="748"/>
        <source>Last time modified</source>
        <translation>Dernière modification</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="760"/>
        <source>Copy path</source>
        <translation>Copier le chemin d’accès</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::FileContentSearchTask</name>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/file-content-search-task.cpp" line="63"/>
        <location filename="../../libsearch/searchinterface/searchtasks/file-content-search-task.cpp" line="89"/>
        <source>File Content</source>
        <translation>Contenu du fichier</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/file-content-search-task.cpp" line="68"/>
        <source>File Content Search</source>
        <translation>Recherche de contenu de fichier</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::FileSearchPlugin</name>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="91"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="249"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="92"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="250"/>
        <source>Open path</source>
        <translation>Chemin ouvert</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="93"/>
        <source>Copy Path</source>
        <translation>Copier le chemin</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="112"/>
        <source>File Search</source>
        <translation>Recherche de fichiers</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="107"/>
        <source>File search.</source>
        <translation>Recherche de fichiers.</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="vanished">Oui</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="155"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="274"/>
        <source>OK</source>
        <translation>Oui</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="157"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="276"/>
        <source>Can not get a default application for opening %1.</source>
        <translation>Impossible d’obtenir une application par défaut pour l’ouverture de %1.</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="185"/>
        <source>File</source>
        <translation>Lime</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="227"/>
        <source>Path</source>
        <translation>Chemin</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="239"/>
        <source>Last time modified</source>
        <translation>Dernière modification</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="251"/>
        <source>Copy path</source>
        <translation>Copier le chemin d’accès</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::MailSearch</name>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="357"/>
        <source>From</source>
        <translation>发件人</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="358"/>
        <source>Time</source>
        <translation>时间</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="359"/>
        <source>To</source>
        <translation>收件人</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="360"/>
        <source>Cc</source>
        <translation>抄送人</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::MailSearchPlugin</name>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="48"/>
        <source>open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="57"/>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="62"/>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="67"/>
        <source>Mail Search</source>
        <translation>邮件搜索</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="114"/>
        <source>Mail</source>
        <translation>邮件</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="251"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::NoteSearch</name>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="243"/>
        <source>Note Description:</source>
        <translatorcomment>便签内容：</translatorcomment>
        <translation>Description de la note :</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::NoteSearchPlugin</name>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="33"/>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="178"/>
        <source>Open</source>
        <translatorcomment>打开</translatorcomment>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="52"/>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="156"/>
        <source>Note Search</source>
        <translatorcomment>便签</translatorcomment>
        <translation>Recherche de notes</translation>
    </message>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="47"/>
        <source>Note Search.</source>
        <translatorcomment>便签.</translatorcomment>
        <translation>Recherche de notes.</translation>
    </message>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="130"/>
        <source>Application</source>
        <translatorcomment>应用</translatorcomment>
        <translation>Application</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SearchManager</name>
    <message>
        <location filename="../../libsearch/index/search-manager.cpp" line="72"/>
        <source>Path:</source>
        <translation>Chemin:</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/search-manager.cpp" line="73"/>
        <source>Modified time:</source>
        <translation>Heure modifiée :</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SearchResultPropertyInfo</name>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="44"/>
        <source>file path</source>
        <translation>chemin d’accès au fichier</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="50"/>
        <source>file name</source>
        <translation>Nom du fichier</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="56"/>
        <source>file icon name</source>
        <translation>Nom de l’icône du fichier</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="62"/>
        <source>modified time</source>
        <translation>Heure modifiée</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="68"/>
        <source>application desktop file path</source>
        <translation>Chemin d’accès au fichier Application Desktop</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="74"/>
        <source>application local name</source>
        <translation>Nom local de l’application</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="80"/>
        <source>application icon name</source>
        <translation>Nom de l’icône de l’application</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="86"/>
        <source>application description</source>
        <translation>Description de l’application</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="92"/>
        <source>is online application</source>
        <translation>est une demande en ligne</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="98"/>
        <source>application package name</source>
        <translation>Nom du package d’application</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SettingsSearchPlugin</name>
    <message>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="37"/>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="177"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="57"/>
        <source>Settings Search</source>
        <translation>Recherche de paramètres</translation>
    </message>
    <message>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="52"/>
        <source>Settings search.</source>
        <translation>Recherche de paramètres.</translation>
    </message>
    <message>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="166"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::UkuiSearchTask</name>
    <message>
        <location filename="../../libsearch/searchinterface/ukui-search-task.cpp" line="124"/>
        <source>Current task uuid error or an unregistered plugin is used!</source>
        <translation>Erreur uuid de la tâche en cours ou un plugin non enregistré est utilisé !</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::WebSearchPlugin</name>
    <message>
        <location filename="../../libsearch/websearch/web-search-plugin.cpp" line="46"/>
        <location filename="../../libsearch/websearch/web-search-plugin.cpp" line="190"/>
        <source>Start browser search</source>
        <translation>Lancer la recherche dans le navigateur</translation>
    </message>
    <message>
        <location filename="../../libsearch/websearch/web-search-plugin.cpp" line="58"/>
        <location filename="../../libsearch/websearch/web-search-plugin.cpp" line="63"/>
        <source>Web Page</source>
        <translation>Page Web</translation>
    </message>
</context>
<context>
    <name>search</name>
    <message>
        <source>search</source>
        <translatorcomment>全局搜索</translatorcomment>
        <translation type="vanished">全局搜索</translation>
    </message>
</context>
</TS>
