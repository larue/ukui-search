<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>QObject</name>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/file-content-search-task.cpp" line="140"/>
        <source>Content index incomplete.</source>
        <translation>ᠠᠭᠤᠯᠭ᠎ᠠ᠎ᠶᠢᠨ ᠬᠡᠯᠬᠢᠶᠡᠰᠦ ᠪᠦᠷᠢᠨ ᠪᠤᠰᠤ ᠃</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/file-search-task.cpp" line="150"/>
        <source>Warning, Can not find home path.</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠦᠯ ᠂home ᠵᠠᠮ᠎ᠢ ᠤᠯᠵᠤ ᠮᠡᠳᠡᠭᠰᠡᠨ ᠦᠬᠡᠢ᠃</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::AiSearchPlugin</name>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="44"/>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="220"/>
        <source>Open</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="45"/>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="221"/>
        <source>Open path</source>
        <translation>ᠹᠠᠢᠯ᠎ᠤᠨ ᠪᠠᠢᠭ᠎ᠠ ᠵᠠᠮ᠎ᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="46"/>
        <source>Copy Path</source>
        <translation>ᠵᠠᠮ᠎ᠢ ᠬᠠᠭᠤᠯᠪᠤᠷᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="70"/>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="75"/>
        <source>AI Search</source>
        <translation>AI ᠡᠷᠢᠬᠦ ᠃</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="113"/>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="243"/>
        <source>OK</source>
        <translation>ᠪᠠᠰᠠ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="115"/>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="245"/>
        <source>Can not get a default application for opening %1.</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠨᠡᠬᠡᠬᠡᠬᠦ᠌%1᠎ᠶᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠯᠳᠡ᠎ᠶᠢ ᠡᠷᠢᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="160"/>
        <source>File</source>
        <translation>ᠹᠠᠢᠯ</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="198"/>
        <source>Path</source>
        <translation>ᠵᠠᠮ</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="210"/>
        <source>Last time modified</source>
        <translation>ᠳᠡᠭᠡᠷ᠎ᠡ ᠤᠳᠠᠭ᠎ᠠ᠎ᠶᠢᠨ ᠵᠠᠰᠠᠭᠰᠠᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../libsearch/aisearch/ai-search-plugin.cpp" line="222"/>
        <source>Copy path</source>
        <translation>ᠵᠠᠮ᠎ᠢ ᠬᠠᠭᠤᠯᠪᠤᠷᠢᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::AppMatch</name>
    <message>
        <source>Application Description:</source>
        <translation type="vanished">应用描述：</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::AppSearch</name>
    <message>
        <source>Application Description:</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠯᠳᠡ᠎ᠶᠢᠨ ᠳᠦᠷᠰᠦᠯᠡᠯ ᠄</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::AppSearchPlugin</name>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="31"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="248"/>
        <source>Open</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="32"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="249"/>
        <source>Add Shortcut to Desktop</source>
        <translation>ᠱᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ᠎ᠤᠨ ᠲᠦᠳᠡ ᠴᠦᠷᠬᠡ᠎ᠶᠢᠨ ᠠᠷᠭ᠎ᠠ᠎ᠳᠤ ᠨᠡᠮᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="33"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="250"/>
        <source>Add Shortcut to Panel</source>
        <translation>ᠡᠬᠦᠷᠭᠡ᠎ᠶᠢᠨ ᠬᠡᠷᠡᠭᠰᠡᠬᠡ᠎ᠶᠢᠨ ᠲᠦᠲᠡ ᠴᠦᠷᠬᠡ᠎ᠳᠦ ᠨᠡᠮᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="34"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="251"/>
        <source>Install</source>
        <translation>ᠤᠭᠰᠠᠷᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="64"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="69"/>
        <source>Applications Search</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠯᠳᠡ</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="154"/>
        <source>Application</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠯᠳᠡ</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="199"/>
        <source>Application Description:</source>
        <translation>应用描述：</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::AppSearchTask</name>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/app-search-task.cpp" line="55"/>
        <source>Application</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠯᠳᠡ</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/app-search-task.cpp" line="60"/>
        <source>Application search.</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠦ ᠡᠷᠢᠯᠲᠡ ᠃</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::DirSearchPlugin</name>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="311"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="462"/>
        <source>Open</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="312"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="463"/>
        <source>Open path</source>
        <translation>ᠹᠠᠢᠯ᠎ᠤᠨ ᠪᠠᠢᠭ᠎ᠠ ᠵᠠᠮ᠎ᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="313"/>
        <source>Copy Path</source>
        <translation>ᠵᠠᠮ᠎ᠢ ᠬᠠᠭᠤᠯᠪᠤᠷᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="332"/>
        <source>Dir Search</source>
        <translation>ᠭᠠᠷᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="398"/>
        <source>Directory</source>
        <translation>ᠭᠠᠷᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="327"/>
        <source>Dir search.</source>
        <translation>ᠭᠠᠷᠴᠠᠭ᠎ᠤᠨ ᠬᠠᠢᠯᠲᠠ</translation>
    </message>
    <message>
        <source>directory</source>
        <translation type="vanished">目录</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="440"/>
        <source>Path</source>
        <translation>ᠵᠠᠮ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="452"/>
        <source>Last time modified</source>
        <translation>ᠳᠡᠭᠡᠷ᠎ᠡ ᠤᠳᠠᠭ᠎ᠠ᠎ᠶᠢᠨ ᠵᠠᠰᠠᠭᠰᠠᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="464"/>
        <source>Copy path</source>
        <translation>ᠵᠠᠮ᠎ᠢ ᠬᠠᠭᠤᠯᠪᠤᠷᠢᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::FileContengSearchPlugin</name>
    <message>
        <source>Open</source>
        <translation type="vanished">ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <source>Open path</source>
        <translation type="vanished">ᠹᠠᠢᠯ᠎ᠤᠨ ᠪᠠᠢᠭ᠎ᠠ ᠵᠠᠮ᠎ᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <source>Copy Path</source>
        <translation type="vanished">ᠹᠠᠢᠯ᠎ᠤᠨ ᠵᠠᠮ᠎ᠢ ᠬᠠᠭᠤᠯᠪᠤᠷᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>File Content Search</source>
        <translation type="vanished">文本内容搜索</translation>
    </message>
    <message>
        <source>File content search.</source>
        <translation type="vanished">ᠲᠸᠺᠰᠲ᠎ᠦᠨ ᠠᠭᠤᠯᠭ᠎ᠠ᠎ᠶᠢ ᠬᠠᠢᠬᠤ᠃</translation>
    </message>
    <message>
        <source>File content search</source>
        <translation type="vanished">ᠲᠸᠺᠰᠲ᠎ᠦᠨ ᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <source>OCR</source>
        <translation type="vanished">OCR</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="vanished">ᠹᠠᠢᠯ</translation>
    </message>
    <message>
        <source>Path</source>
        <translation type="vanished">ᠵᠠᠮ</translation>
    </message>
    <message>
        <source>Last time modified</source>
        <translation type="vanished">ᠳᠡᠭᠡᠷ᠎ᠡ ᠤᠳᠠᠭ᠎ᠠ᠎ᠶᠢᠨ ᠵᠠᠰᠠᠭᠰᠠᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <source>Copy path</source>
        <translation type="vanished">ᠹᠠᠢᠯ᠎ᠤᠨ ᠵᠠᠮ᠎ᠢ ᠬᠠᠭᠤᠯᠪᠤᠷᠢᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::FileContentSearchPlugin</name>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="508"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="758"/>
        <source>Open</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="509"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="759"/>
        <source>Open path</source>
        <translation>ᠹᠠᠢᠯ᠎ᠤᠨ ᠪᠠᠢᠭ᠎ᠠ ᠵᠠᠮ᠎ᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="510"/>
        <source>Copy Path</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠠᠷᠭ᠎ᠠ ᠵᠠᠮ ᠃</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="533"/>
        <source>File content search.</source>
        <translation>ᠲᠸᠺᠰᠲ᠎ᠦᠨ ᠠᠭᠤᠯᠭ᠎ᠠ᠎ᠶᠢ ᠬᠠᠢᠬᠤ᠃</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="538"/>
        <source>File content search</source>
        <translation>ᠲᠸᠺᠰᠲ᠎ᠦᠨ ᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="622"/>
        <source>OCR</source>
        <translation>OCR</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="626"/>
        <source>File</source>
        <translation>ᠹᠠᠢᠯ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="736"/>
        <source>Path</source>
        <translation>ᠵᠠᠮ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="748"/>
        <source>Last time modified</source>
        <translation>ᠳᠡᠭᠡᠷ᠎ᠡ ᠤᠳᠠᠭ᠎ᠠ᠎ᠶᠢᠨ ᠵᠠᠰᠠᠭᠰᠠᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="760"/>
        <source>Copy path</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠠᠷᠭ᠎ᠠ ᠵᠠᠮ ᠃</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::FileContentSearchTask</name>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/file-content-search-task.cpp" line="63"/>
        <location filename="../../libsearch/searchinterface/searchtasks/file-content-search-task.cpp" line="89"/>
        <source>File Content</source>
        <translation>ᠲᠸᠺᠰᠲ᠎ᠦᠨ ᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/file-content-search-task.cpp" line="68"/>
        <source>File Content Search</source>
        <translation>ᠲᠸᠺᠰᠲ᠎ᠦᠨ ᠠᠭᠤᠯᠭ᠎ᠠ᠎ᠶᠢ ᠬᠠᠢᠬᠤ᠃</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::FileSearchPlugin</name>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="91"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="249"/>
        <source>Open</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="92"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="250"/>
        <source>Open path</source>
        <translation>ᠹᠠᠢᠯ᠎ᠤᠨ ᠪᠠᠢᠭ᠎ᠠ ᠵᠠᠮ᠎ᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="93"/>
        <source>Copy Path</source>
        <translation>ᠵᠠᠮ᠎ᠢ ᠬᠠᠭᠤᠯᠪᠤᠷᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="112"/>
        <source>File Search</source>
        <translation>ᠹᠠᠢᠯ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="107"/>
        <source>File search.</source>
        <translation>ᠹᠠᠢᠯ᠎ᠤᠨ ᠬᠠᠢᠯᠲᠠ</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="vanished">Yes</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="155"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="274"/>
        <source>OK</source>
        <translation>ᠪᠣᠯᠣᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="157"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="276"/>
        <source>Can not get a default application for opening %1.</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠨᠡᠬᠡᠬᠡᠬᠦ᠌%1᠎ᠶᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠯᠳᠡ᠎ᠶᠢ ᠡᠷᠢᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="185"/>
        <source>File</source>
        <translation>ᠹᠠᠢᠯ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="227"/>
        <source>Path</source>
        <translation>ᠵᠠᠮ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="239"/>
        <source>Last time modified</source>
        <translation>ᠳᠡᠭᠡᠷ᠎ᠡ ᠤᠳᠠᠭ᠎ᠠ᠎ᠶᠢᠨ ᠵᠠᠰᠠᠭᠰᠠᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="251"/>
        <source>Copy path</source>
        <translation>ᠵᠠᠮ᠎ᠢ ᠬᠠᠭᠤᠯᠪᠤᠷᠢᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::MailSearch</name>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="357"/>
        <source>From</source>
        <translation>ᠢᠮᠸᠯ ᠢᠯᠡᠬᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="358"/>
        <source>Time</source>
        <translation>ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="359"/>
        <source>To</source>
        <translation>ᠢᠮᠸᠯ ᠬᠤᠷᠢᠶᠠᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="360"/>
        <source>Cc</source>
        <translation>ᠬᠠᠭᠤᠯᠵᠤ ᠬᠦᠷᠬᠡᠭᠴᠢ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::MailSearchPlugin</name>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="48"/>
        <source>open</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="57"/>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="62"/>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="67"/>
        <source>Mail Search</source>
        <translation>ᠢᠮᠸᠯ ᠬᠠᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="114"/>
        <source>Mail</source>
        <translation>ᠢᠮᠸᠯ</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="251"/>
        <source>Open</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::NoteSearch</name>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="243"/>
        <source>Note Description:</source>
        <translatorcomment>便签内容：</translatorcomment>
        <translation>ᠳᠦᠬᠦᠮ ᠱᠤᠱᠢᠭ᠎ᠠ᠎ᠶᠢᠨ ᠠᠭᠤᠯᠭ᠎ᠠ ᠄</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::NoteSearchPlugin</name>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="33"/>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="178"/>
        <source>Open</source>
        <translatorcomment>打开</translatorcomment>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="52"/>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="156"/>
        <source>Note Search</source>
        <translatorcomment>便签</translatorcomment>
        <translation>ᠳᠦᠬᠦᠮ ᠱᠤᠱᠢᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="47"/>
        <source>Note Search.</source>
        <translatorcomment>便签.</translatorcomment>
        <translation>ᠳᠦᠬᠦᠮ ᠱᠤᠱᠢᠭ᠎ᠠ ᠄</translation>
    </message>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="130"/>
        <source>Application</source>
        <translatorcomment>应用</translatorcomment>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠯᠳᠡ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SearchManager</name>
    <message>
        <location filename="../../libsearch/index/search-manager.cpp" line="72"/>
        <source>Path:</source>
        <translation>ᠵᠠᠮ᠄</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/search-manager.cpp" line="73"/>
        <source>Modified time:</source>
        <translation>ᠵᠠᠰᠠᠭᠰᠠᠨ ᠴᠠᠭ ᠄</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SearchResultPropertyInfo</name>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="44"/>
        <source>file path</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠠᠷᠭ᠎ᠠ ᠵᠠᠮ</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="50"/>
        <source>file name</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="56"/>
        <source>file icon name</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠵᠢᠷᠤᠭ ᠤᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="62"/>
        <source>modified time</source>
        <translation>ᠴᠠᠭ ᠢᠶᠠᠨ ᠵᠠᠰᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="68"/>
        <source>application desktop file path</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠦ ᠫᠷᠦᠭᠷᠠᠮ ᠤᠨ ᠰᠢᠷᠡᠭᠡᠨ ᠦ ᠨᠢᠭᠤᠷ ᠤᠨ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠠᠷᠭ᠎ᠠ ᠵᠠᠮ</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="74"/>
        <source>application local name</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠦ ᠫᠷᠦᠭᠷᠠᠮ ᠲᠤᠰ ᠭᠠᠵᠠᠷ ᠤᠨ ᠨᠡᠷᠡᠶᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="80"/>
        <source>application icon name</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠦ ᠫᠷᠦᠭᠷᠠᠮ ᠤᠨ ᠵᠢᠷᠤᠭ ᠤᠨ ᠵᠢᠷᠤᠭ ᠤᠨ ᠨᠡᠷᠡᠶᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="86"/>
        <source>application description</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠦ ᠲᠣᠳᠣᠷᠬᠠᠶᠢᠯᠠᠯᠲᠠ</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="92"/>
        <source>is online application</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠬᠡᠷᠡᠭᠯᠡᠯᠲᠡ ᠮᠥᠨ</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="98"/>
        <source>application package name</source>
        <translation>ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ ᠤᠨ ᠪᠣᠭᠴᠣ ᠶᠢᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SearchTaskPluginManager</name>
    <message>
        <source>plugin type: %1, is disabled!</source>
        <translation type="vanished">ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠲᠤᠨᠤᠭ᠎ᠤᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠮᠠᠶᠢᠭ ᠄%1 ᠂ ᠨᠢᠭᠡᠨᠳᠡ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ᠌᠎ᠶᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠪᠠ!</translation>
    </message>
    <message>
        <source>plugin type: %1, is not registered!</source>
        <translation type="vanished">ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠲᠤᠨᠤᠭ᠎ᠤᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠮᠠᠶᠢᠭ ᠄%1 ᠂ ᠪᠦᠷᠢᠳᠭᠡᠬᠦᠯᠦᠬᠡ ᠦᠬᠡᠢ!</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SettingsSearchPlugin</name>
    <message>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="37"/>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="177"/>
        <source>Open</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="57"/>
        <source>Settings Search</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠯ ᠬᠡᠱᠢᠬᠦᠨ</translation>
    </message>
    <message>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="52"/>
        <source>Settings search.</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠯ ᠬᠡᠱᠢᠬᠦᠨ᠎ᠦ ᠬᠠᠢᠯᠲᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="166"/>
        <source>Settings</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠵᠦᠢᠯ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::UkuiSearchTask</name>
    <message>
        <location filename="../../libsearch/searchinterface/ukui-search-task.cpp" line="124"/>
        <source>Current task uuid error or an unregistered plugin is used!</source>
        <translation>ᠲᠤᠰ ᠡᠬᠦᠷᠭᠡuuid ᠨᠢ ᠪᠤᠷᠤᠭᠤ ᠡᠰᠡᠪᠡᠯ ᠨᠢᠭᠡ ᠪᠦᠷᠢᠳᠭᠡᠬᠦᠯᠦᠭᠰᠡᠨ ᠦᠬᠡᠢ ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠲᠤᠨᠤᠭ ᠬᠡᠷᠡᠭᠯᠡᠭᠳᠡᠭᠰᠡᠨ ᠪᠠᠢᠨ᠎ᠠ!</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::UkuiSearchTaskPrivate</name>
    <message>
        <source>Current task uuid error or an unregistered plugin is used!</source>
        <translation type="vanished">ᠲᠤᠰ ᠡᠬᠦᠷᠭᠡuuid ᠨᠢ ᠪᠤᠷᠤᠭᠤ ᠡᠰᠡᠪᠡᠯ ᠨᠢᠭᠡ ᠪᠦᠷᠢᠳᠭᠡᠬᠦᠯᠦᠭᠰᠡᠨ ᠦᠬᠡᠢ ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠲᠤᠨᠤᠭ ᠬᠡᠷᠡᠭᠯᠡᠭᠳᠡᠭᠰᠡᠨ ᠪᠠᠢᠨ᠎ᠠ!</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::WebSearchPlugin</name>
    <message>
        <location filename="../../libsearch/websearch/web-search-plugin.cpp" line="46"/>
        <location filename="../../libsearch/websearch/web-search-plugin.cpp" line="190"/>
        <source>Start browser search</source>
        <translation>ᠬᠠᠢᠭᠤᠷ᠎ᠤᠨ ᠬᠠᠢᠯᠲᠠ᠎ᠶᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../../libsearch/websearch/web-search-plugin.cpp" line="58"/>
        <location filename="../../libsearch/websearch/web-search-plugin.cpp" line="63"/>
        <source>Web Page</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠨᠢᠭᠤᠷ</translation>
    </message>
</context>
</TS>
