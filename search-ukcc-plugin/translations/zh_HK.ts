<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>Search</name>
    <message>
        <location filename="../search.cpp" line="41"/>
        <location filename="../search.cpp" line="279"/>
        <source>Search</source>
        <translation>全域搜索</translation>
        <extra-contents_path>/Search/Search</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="284"/>
        <source>Default web searching engine</source>
        <translation>默認互聯網搜尋引擎</translation>
        <extra-contents_path>/Search/Default web searching engine</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="288"/>
        <source>baidu</source>
        <translation>百度</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="289"/>
        <source>sougou</source>
        <translation>搜狗</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="290"/>
        <source>360</source>
        <translation>360</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="296"/>
        <source>Create index</source>
        <translation>創建索引</translation>
        <extra-contents_path>/Search/Create index</extra-contents_path>
    </message>
    <message>
        <source>Creating index can help you getting results quickly.</source>
        <translation type="vanished">創建索引可以快速獲取搜尋結果</translation>
    </message>
    <message>
        <source>File Content Search</source>
        <translation type="vanished">搜索文字內容</translation>
        <extra-contents_path>/Search/File Content Search</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="309"/>
        <source>Create file index</source>
        <translation>創建檔案索引</translation>
        <extra-contents_path>/Search/Create file index</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="334"/>
        <source>Create file content index</source>
        <translation>創建檔案內容索引</translation>
        <extra-contents_path>/Search/Create file content index</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="366"/>
        <source>Precise Search</source>
        <translation>精確搜索</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="369"/>
        <source>show the results that exactly match the keyword</source>
        <translation>僅顯示與輸入內容完全一致的搜尋結果</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="379"/>
        <source>Fuzzy Search</source>
        <translation>模糊搜索</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="382"/>
        <source>show more results that match the keyword</source>
        <translation>顯示更多與輸入內容匹配的搜尋結果</translation>
    </message>
    <message>
        <source>Create ai index</source>
        <translation type="vanished">創建Ai索引</translation>
        <extra-contents_path>/Search/Create ai index</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="454"/>
        <source>Search Folders</source>
        <translation>搜索範圍</translation>
        <extra-contents_path>/Search/Search Folders</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="460"/>
        <source>Following folders will be searched. You can set it by adding and removing folders.</source>
        <translation>以下文件的內容將出現在全域搜索的結果中</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="487"/>
        <source>Block Folders</source>
        <translation>排除的資料夾</translation>
        <extra-contents_path>/Search/Block Folders</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="492"/>
        <source>Following folders will not be searched. You can set it by adding and removing folders.</source>
        <translation>搜索將不查看以下資料夾，通過添加和刪除可以設置排除的資料夾位置.</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="840"/>
        <location filename="../search.cpp" line="893"/>
        <source>Directories</source>
        <translation>資料夾</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="841"/>
        <source>select blocked folder</source>
        <translation>選擇排除的資料夾</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="842"/>
        <location filename="../search.cpp" line="895"/>
        <source>Select</source>
        <translation>選擇</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="843"/>
        <location filename="../search.cpp" line="896"/>
        <source>Position: </source>
        <translation>位置</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="844"/>
        <location filename="../search.cpp" line="897"/>
        <source>FileName: </source>
        <translation>檔名</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="845"/>
        <location filename="../search.cpp" line="898"/>
        <source>FileType: </source>
        <translation>類型</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="198"/>
        <location filename="../search.cpp" line="846"/>
        <location filename="../search.cpp" line="899"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="196"/>
        <source>Create file content index will increase the usage of memory and CPU, do you still want to open it?</source>
        <translation>創建文件內容索引會影響記憶體和CPU佔用率，是否開啟？</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="197"/>
        <source>OK</source>
        <translation>開啟</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="415"/>
        <source>Index the text in pictures</source>
        <translation>索引圖片中的文字</translation>
        <extra-contents_path>/Search/Use OCR to index the text in pictures</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="443"/>
        <source>Create AI index</source>
        <translation>創建AI索引</translation>
        <extra-contents_path>/Search/Create AI index</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="861"/>
        <location filename="../search.cpp" line="865"/>
        <location filename="../search.cpp" line="869"/>
        <location filename="../search.cpp" line="873"/>
        <location filename="../search.cpp" line="912"/>
        <location filename="../search.cpp" line="915"/>
        <location filename="../search.cpp" line="918"/>
        <location filename="../search.cpp" line="921"/>
        <location filename="../search.cpp" line="924"/>
        <location filename="../search.cpp" line="927"/>
        <location filename="../search.cpp" line="930"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="861"/>
        <source>Add blocked folder failed, its parent dir has been added!</source>
        <translation>添加失敗，父目錄已被添加！</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="865"/>
        <source>Add blocked folder failed, choosen path is not exist!</source>
        <translation>添加失敗，要添加的路徑不存在！</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="869"/>
        <source>Add blocked folder failed, it has already been blocked!</source>
        <translation>添加失敗，這個資料夾已經被添加過了！</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="873"/>
        <location filename="../search.cpp" line="927"/>
        <source>Add search folder failed, hidden&#x3000;path is not supported!</source>
        <translation>添加失敗，不支持隱藏目錄！</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="894"/>
        <source>select search folder</source>
        <translation>選擇要搜索的資料夾</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="912"/>
        <source>Add search folder failed, choosen path or its parent dir has been added!</source>
        <translation>添加失敗，該目錄或其父目錄已被添加！</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="915"/>
        <source>Add search folder failed, choosen path is not supported currently!</source>
        <translation>添加失敗，暫不支援該目錄！</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="918"/>
        <source>Add search folder failed, choosen path is in repeat mounted devices and another path which is in the same device has been added!</source>
        <translation>添加失敗，資料夾位於重複掛載設備下，且該設備另一個掛載點的資料夾已被添加！</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="921"/>
        <source>Add search folder failed, another path which is in the same device has been added!</source>
        <translation>&gt;添加失敗，資料夾位於重複掛載設備下，相同內容的資料夾已被添加！</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="924"/>
        <source>Add search folder failed, choosen path is not exists!</source>
        <translation>添加失敗，要添加的路徑不存在！</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="930"/>
        <source>Add search folder failed, permission denied!</source>
        <translation>添加失敗，該目錄無許可權訪問！</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="711"/>
        <location filename="../search.cpp" line="782"/>
        <source>delete</source>
        <translation>刪除</translation>
    </message>
</context>
</TS>
