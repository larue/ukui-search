<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>Search</name>
    <message>
        <location filename="../search.cpp" line="41"/>
        <location filename="../search.cpp" line="279"/>
        <source>Search</source>
        <translation>Buscar</translation>
        <extra-contents_path>/Search/Search</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="296"/>
        <source>Create index</source>
        <translation>Crear índice</translation>
        <extra-contents_path>/Search/Create index</extra-contents_path>
    </message>
    <message>
        <source>Creating index can help you getting results quickly.</source>
        <translation type="vanished">La creación de índices puede ayudarlo a obtener resultados rápidamente.</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="284"/>
        <source>Default web searching engine</source>
        <translation>Motor de búsqueda web predeterminado</translation>
        <extra-contents_path>/Search/Default web searching engine</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="288"/>
        <source>baidu</source>
        <translation>Baidu</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="289"/>
        <source>sougou</source>
        <translation>Sougou</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="290"/>
        <source>360</source>
        <translation>360</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="443"/>
        <source>Create AI index</source>
        <translation>Crear un índice AI</translation>
        <extra-contents_path>/Search/Create AI index</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="487"/>
        <source>Block Folders</source>
        <translation>Carpetas de bloques</translation>
        <extra-contents_path>/Search/Block Folders</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="492"/>
        <source>Following folders will not be searched. You can set it by adding and removing folders.</source>
        <translation>No se buscarán las siguientes carpetas. Puede configurarlo agregando y eliminando carpetas.</translation>
    </message>
    <message>
        <source>Choose folder</source>
        <translation type="vanished">添加</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="711"/>
        <location filename="../search.cpp" line="782"/>
        <source>delete</source>
        <translation>borrar</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="840"/>
        <location filename="../search.cpp" line="893"/>
        <source>Directories</source>
        <translation>Directorios</translation>
    </message>
    <message>
        <source>File Content Search</source>
        <translation type="vanished">Búsqueda de contenido de archivo</translation>
        <extra-contents_path>/Search/File Content Search</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="382"/>
        <source>show more results that match the keyword</source>
        <translation>Mostrar más resultados que coincidan con la palabra clave</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="379"/>
        <source>Fuzzy Search</source>
        <translation>Búsqueda aproximada</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="196"/>
        <source>Create file content index will increase the usage of memory and CPU, do you still want to open it?</source>
        <translation>Crear índice de contenido de archivo aumentará el uso de memoria y CPU, ¿aún desea abrirlo?</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="197"/>
        <source>OK</source>
        <translation>De acuerdo</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="309"/>
        <source>Create file index</source>
        <translation>Crear índice de archivos</translation>
        <extra-contents_path>/Search/Create file index</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="334"/>
        <source>Create file content index</source>
        <translation>Crear índice de contenido de archivo</translation>
        <extra-contents_path>/Search/Create file content index</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="366"/>
        <source>Precise Search</source>
        <translation>Búsqueda precisa</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="369"/>
        <source>show the results that exactly match the keyword</source>
        <translation>Mostrar los resultados que coincidan exactamente con la palabra clave</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="415"/>
        <source>Index the text in pictures</source>
        <translation>Indexar el texto en imágenes</translation>
        <extra-contents_path>/Search/Use OCR to index the text in pictures</extra-contents_path>
    </message>
    <message>
        <source>Create ai index</source>
        <translation type="vanished">Crear un índice ai</translation>
        <extra-contents_path>/Search/Create ai index</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="454"/>
        <source>Search Folders</source>
        <translation>Carpetas de búsqueda</translation>
        <extra-contents_path>/Search/Search Folders</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="460"/>
        <source>Following folders will be searched. You can set it by adding and removing folders.</source>
        <translation>Se buscarán las siguientes carpetas. Puede configurarlo agregando y eliminando carpetas.</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="841"/>
        <source>select blocked folder</source>
        <translation>Seleccione la carpeta bloqueada</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="842"/>
        <location filename="../search.cpp" line="895"/>
        <source>Select</source>
        <translation>Escoger</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="843"/>
        <location filename="../search.cpp" line="896"/>
        <source>Position: </source>
        <translation>Posición: </translation>
    </message>
    <message>
        <location filename="../search.cpp" line="844"/>
        <location filename="../search.cpp" line="897"/>
        <source>FileName: </source>
        <translation>Nombre: </translation>
    </message>
    <message>
        <location filename="../search.cpp" line="845"/>
        <location filename="../search.cpp" line="898"/>
        <source>FileType: </source>
        <translation>Tipo de archivo: </translation>
    </message>
    <message>
        <location filename="../search.cpp" line="198"/>
        <location filename="../search.cpp" line="846"/>
        <location filename="../search.cpp" line="899"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="861"/>
        <location filename="../search.cpp" line="865"/>
        <location filename="../search.cpp" line="869"/>
        <location filename="../search.cpp" line="873"/>
        <location filename="../search.cpp" line="912"/>
        <location filename="../search.cpp" line="915"/>
        <location filename="../search.cpp" line="918"/>
        <location filename="../search.cpp" line="921"/>
        <location filename="../search.cpp" line="924"/>
        <location filename="../search.cpp" line="927"/>
        <location filename="../search.cpp" line="930"/>
        <source>Warning</source>
        <translation>Advertencia</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="873"/>
        <location filename="../search.cpp" line="927"/>
        <source>Add search folder failed, hidden&#x3000;path is not supported!</source>
        <translation>Error al agregar carpeta de búsqueda, ¡no se admite la ruta oculta!</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="930"/>
        <source>Add search folder failed, permission denied!</source>
        <translation>¡Error al agregar carpeta de búsqueda, permiso denegado!</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="861"/>
        <source>Add blocked folder failed, its parent dir has been added!</source>
        <translation>Error al agregar carpeta bloqueada, ¡se ha agregado su directorio principal!</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="865"/>
        <source>Add blocked folder failed, choosen path is not exist!</source>
        <translation>¡Agregar carpeta bloqueada falló, la ruta elegida no existe!</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="869"/>
        <source>Add blocked folder failed, it has already been blocked!</source>
        <translation>Error al agregar carpeta bloqueada, ¡ya ha sido bloqueada!</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="894"/>
        <source>select search folder</source>
        <translation>Seleccione la carpeta de búsqueda</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="915"/>
        <source>Add search folder failed, choosen path is not supported currently!</source>
        <translation>Error al agregar carpeta de búsqueda, la ruta elegida no es compatible actualmente.</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="921"/>
        <source>Add search folder failed, another path which is in the same device has been added!</source>
        <translation>Error al agregar carpeta de búsqueda, se ha agregado otra ruta que está en el mismo dispositivo.</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="912"/>
        <source>Add search folder failed, choosen path or its parent dir has been added!</source>
        <translation>¡Error al agregar carpeta de búsqueda, se eligió la ruta o se agregó su directorio principal!</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="918"/>
        <source>Add search folder failed, choosen path is in repeat mounted devices and another path which is in the same device has been added!</source>
        <translation>Error al agregar carpeta de búsqueda, la ruta elegida está en dispositivos montados repetidamente y se ha agregado otra ruta que está en el mismo dispositivo.</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="924"/>
        <source>Add search folder failed, choosen path is not exists!</source>
        <translation>Error al agregar carpeta de búsqueda, la ruta elegida no existe.</translation>
    </message>
</context>
</TS>
