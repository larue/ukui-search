<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>Search</name>
    <message>
        <location filename="../search.cpp" line="41"/>
        <location filename="../search.cpp" line="279"/>
        <source>Search</source>
        <translation>Search</translation>
        <extra-contents_path>/Search/Search</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="284"/>
        <source>Default web searching engine</source>
        <translation>Default web searching engine</translation>
        <extra-contents_path>/Search/Default web searching engine</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="288"/>
        <source>baidu</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="289"/>
        <source>sougou</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="290"/>
        <source>360</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="296"/>
        <source>Create index</source>
        <translation>Create index</translation>
        <extra-contents_path>/Search/Create index</extra-contents_path>
    </message>
    <message>
        <source>File Content Search</source>
        <translation type="vanished">File Content Search</translation>
        <extra-contents_path>/Search/File Content Search</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="366"/>
        <source>Precise Search</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="369"/>
        <source>show the results that exactly match the keyword</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="379"/>
        <source>Fuzzy Search</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="382"/>
        <source>show more results that match the keyword</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="415"/>
        <source>Index the text in pictures</source>
        <translation></translation>
        <extra-contents_path>/Search/Use OCR to index the text in pictures</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="454"/>
        <source>Search Folders</source>
        <translation>Search Folders</translation>
        <extra-contents_path>/Search/Search Folders</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="460"/>
        <source>Following folders will be searched. You can set it by adding and removing folders.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="487"/>
        <source>Block Folders</source>
        <translation>Block Folders</translation>
        <extra-contents_path>/Search/Block Folders</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="492"/>
        <source>Following folders will not be searched. You can set it by adding and removing folders.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="840"/>
        <location filename="../search.cpp" line="893"/>
        <source>Directories</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="841"/>
        <source>select blocked folder</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="842"/>
        <location filename="../search.cpp" line="895"/>
        <source>Select</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="843"/>
        <location filename="../search.cpp" line="896"/>
        <source>Position: </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="844"/>
        <location filename="../search.cpp" line="897"/>
        <source>FileName: </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="845"/>
        <location filename="../search.cpp" line="898"/>
        <source>FileType: </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="198"/>
        <location filename="../search.cpp" line="846"/>
        <location filename="../search.cpp" line="899"/>
        <source>Cancel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="196"/>
        <source>Create file content index will increase the usage of memory and CPU, do you still want to open it?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="197"/>
        <source>OK</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="443"/>
        <source>Create AI index</source>
        <translation></translation>
        <extra-contents_path>/Search/Create AI index</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="861"/>
        <location filename="../search.cpp" line="865"/>
        <location filename="../search.cpp" line="869"/>
        <location filename="../search.cpp" line="873"/>
        <location filename="../search.cpp" line="912"/>
        <location filename="../search.cpp" line="915"/>
        <location filename="../search.cpp" line="918"/>
        <location filename="../search.cpp" line="921"/>
        <location filename="../search.cpp" line="924"/>
        <location filename="../search.cpp" line="927"/>
        <location filename="../search.cpp" line="930"/>
        <source>Warning</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="861"/>
        <source>Add blocked folder failed, its parent dir has been added!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="865"/>
        <source>Add blocked folder failed, choosen path is not exist!</source>
        <translation>Add blocked folder failed, choosen path is not exist!</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="869"/>
        <source>Add blocked folder failed, it has already been blocked!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="894"/>
        <source>select search folder</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="912"/>
        <source>Add search folder failed, choosen path or its parent dir has been added!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="915"/>
        <source>Add search folder failed, choosen path is not supported currently!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="918"/>
        <source>Add search folder failed, choosen path is in repeat mounted devices and another path which is in the same device has been added!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="921"/>
        <source>Add search folder failed, another path which is in the same device has been added!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="924"/>
        <source>Add search folder failed, choosen path is not exists!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="873"/>
        <location filename="../search.cpp" line="927"/>
        <source>Add search folder failed, hidden&#x3000;path is not supported!</source>
        <translation>Add search folder failed, hidden&#x3000;path is not supported!</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="309"/>
        <source>Create file index</source>
        <translation>Create file index</translation>
        <extra-contents_path>/Search/Create file index</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="334"/>
        <source>Create file content index</source>
        <translation>Create file content index</translation>
        <extra-contents_path>/Search/Create file content index</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="930"/>
        <source>Add search folder failed, permission denied!</source>
        <translation>Add search folder failed, permission denied!</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="711"/>
        <location filename="../search.cpp" line="782"/>
        <source>delete</source>
        <translation></translation>
    </message>
</context>
</TS>
