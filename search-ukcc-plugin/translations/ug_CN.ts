<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ug">
<context>
    <name>Search</name>
    <message>
        <location filename="../search.cpp" line="41"/>
        <location filename="../search.cpp" line="279"/>
        <source>Search</source>
        <translation>ئىزدە</translation>
        <extra-contents_path>/Search/Search</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="296"/>
        <source>Create index</source>
        <translation>كۆرسەتكۈچ قۇرۇش</translation>
        <extra-contents_path>/Search/Create index</extra-contents_path>
    </message>
    <message>
        <source>Creating index can help you getting results quickly.</source>
        <translation type="vanished">كۆرسەتكۈچ قۇرۇش سىزنىڭ تېز سۈرئەتتە نەتىجە ئېلىشىڭىزغا ياردەم بېرىدۇ.</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="284"/>
        <source>Default web searching engine</source>
        <translation>كۆڭۈلدىكى تور ئىزدەش ماتورى</translation>
        <extra-contents_path>/Search/Default web searching engine</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="288"/>
        <source>baidu</source>
        <translation>بەيدۇ</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="289"/>
        <source>sougou</source>
        <translation>sougou</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="290"/>
        <source>360</source>
        <translation>360</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="443"/>
        <source>Create AI index</source>
        <translation>AI كۆرسەتكۈچى قۇرۇش</translation>
        <extra-contents_path>/Search/Create AI index</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="487"/>
        <source>Block Folders</source>
        <translation>ھۆججەت قىسقۇچلارنى توسۇش</translation>
        <extra-contents_path>/Search/Block Folders</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="492"/>
        <source>Following folders will not be searched. You can set it by adding and removing folders.</source>
        <translation>تۆۋەندىكى ھۆججەت قىسقۇچلار ئاختۇرۇلمايدۇ. ھۆججەت قىسقۇچلارنى قوشۇش ۋە چىقىرىۋېتىش ئارقىلىق تەڭشەپ قويسىڭىز بولىدۇ.</translation>
    </message>
    <message>
        <source>Choose folder</source>
        <translation type="vanished">ھۆججەت قىسقۇچنى تاللاش</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="711"/>
        <location filename="../search.cpp" line="782"/>
        <source>delete</source>
        <translation>ئۆچۈر</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="840"/>
        <location filename="../search.cpp" line="893"/>
        <source>Directories</source>
        <translation>مۇندەرىجىلەر</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="196"/>
        <source>Create file content index will increase the usage of memory and CPU, do you still want to open it?</source>
        <translation>ھۆججەت مەزمۇن كۆرسەتكۈچى قۇرۇش ئىچكى ساقلىغۇچ ۋە CPU نىڭ ئىشلىتىش مىقدارىنى ئاشۇرىدۇ، يەنە ئاچقۇڭىز بارمۇ؟</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="197"/>
        <source>OK</source>
        <translation>ماقۇل</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="309"/>
        <source>Create file index</source>
        <translation>ھۆججەت كۆرسەتكۈچى قۇرۇش</translation>
        <extra-contents_path>/Search/Create file index</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="334"/>
        <source>Create file content index</source>
        <translation>ھۆججەت مەزمۇنى كۆرسەتكۈچى قۇرۇش</translation>
        <extra-contents_path>/Search/Create file content index</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="366"/>
        <source>Precise Search</source>
        <translation>ئېنىق ئىزدەش</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="369"/>
        <source>show the results that exactly match the keyword</source>
        <translation>ئاچقۇچلۇق سۆزگە تامامەن ماس كېلىدىغان نەتىجىنى كۆرسىتىش</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="379"/>
        <source>Fuzzy Search</source>
        <translation>غۇۋا ئىزدەش</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="382"/>
        <source>show more results that match the keyword</source>
        <translation>ئاچقۇچلۇق سۆزگە ماس كېلىدىغان تېخىمۇ كۆپ نەتىجىلەرنى كۆرسىتىش</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="415"/>
        <source>Index the text in pictures</source>
        <translation>رەسىمدىكى تېكىستلەرنى كۆرسەتكۈچ</translation>
        <extra-contents_path>/Search/Use OCR to index the text in pictures</extra-contents_path>
    </message>
    <message>
        <source>Create ai index</source>
        <translation type="vanished">ai كۆرسەتكۈچى قۇرۇش</translation>
        <extra-contents_path>/Search/Create ai index</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="454"/>
        <source>Search Folders</source>
        <translation>ھۆججەت قىسقۇچلارنى ئىزدەش</translation>
        <extra-contents_path>/Search/Search Folders</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="460"/>
        <source>Following folders will be searched. You can set it by adding and removing folders.</source>
        <translation>تۆۋەندىكى ھۆججەت قىسقۇچلار ئاختۇرۇلىدۇ. ھۆججەت قىسقۇچلارنى قوشۇش ۋە چىقىرىۋېتىش ئارقىلىق تەڭشەپ قويسىڭىز بولىدۇ.</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="841"/>
        <source>select blocked folder</source>
        <translation>چەكلەنگەن ھۆججەت قىسقۇچنى تاللاش</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="842"/>
        <location filename="../search.cpp" line="895"/>
        <source>Select</source>
        <translation>تاللاش</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="843"/>
        <location filename="../search.cpp" line="896"/>
        <source>Position: </source>
        <translation>ئورنى: </translation>
    </message>
    <message>
        <location filename="../search.cpp" line="844"/>
        <location filename="../search.cpp" line="897"/>
        <source>FileName: </source>
        <translation>ھۆججەت نامى: </translation>
    </message>
    <message>
        <location filename="../search.cpp" line="845"/>
        <location filename="../search.cpp" line="898"/>
        <source>FileType: </source>
        <translation>FileType: </translation>
    </message>
    <message>
        <location filename="../search.cpp" line="861"/>
        <source>Add blocked folder failed, its parent dir has been added!</source>
        <translation>چەكلەنگەن ھۆججەت قىسقۇچنى قوشۇش مەغلۇپ بولدى، ئۇنىڭ ئانا دىدارى قوشۇلدى!</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="865"/>
        <source>Add blocked folder failed, choosen path is not exist!</source>
        <translation>چەكلەنگەن ھۆججەت قىسقۇچنى قوشۇش مەغلۇپ بولدى، تاللانغان يول مەۋجۇت ئەمەس!</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="869"/>
        <source>Add blocked folder failed, it has already been blocked!</source>
        <translation>چەكلەنگەن ھۆججەت قىسقۇچنى قوشۇش مەغلۇپ بولدى، ئاللىقاچان چەكلەنگەن!</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="873"/>
        <location filename="../search.cpp" line="927"/>
        <source>Add search folder failed, hidden&#x3000;path is not supported!</source>
        <translation>ئىزدەش ھۆججەت قىسقۇچنى قوشۇش مەغلۇپ بولدى، يوشۇرۇن يول قوللىمايدۇ!</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="894"/>
        <source>select search folder</source>
        <translation>ئىزدەش ھۆججەت قىسقۇچنى تاللاش</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="912"/>
        <source>Add search folder failed, choosen path or its parent dir has been added!</source>
        <translation>ئىزدەش ھۆججەت قىسقۇچنى قوشۇش مەغلۇپ بولدى، تاللانغان يول ياكى ئۇنىڭ ئانا دىتى قوشۇلدى!</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="915"/>
        <source>Add search folder failed, choosen path is not supported currently!</source>
        <translation>ئىزدەش ھۆججەت قىسقۇچنى قوشۇش مەغلۇپ بولدى، تاللانغان يول ھازىر قوللىمايدۇ!</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="918"/>
        <source>Add search folder failed, choosen path is in repeat mounted devices and another path which is in the same device has been added!</source>
        <translation>ئىزدەش ھۆججەت قىسقۇچىنى قوشۇش مەغلۇپ بولدى، تاللانغان يول قايتا-قايتا قاچىلانغان ئۈسكۈنىلەردە ۋە ئوخشاش ئۈسكۈنىدە قاچىلانغان يەنە بىر يول قوشۇلدى!</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="921"/>
        <source>Add search folder failed, another path which is in the same device has been added!</source>
        <translation>ئىزدەش ھۆججەت قىسقۇچىنى قوشۇش مەغلۇپ بولدى، ئوخشاش ئۈسكۈنە ئىچىدە يەنە بىر يول قوشۇلدى!</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="924"/>
        <source>Add search folder failed, choosen path is not exists!</source>
        <translation>ئىزدەش ھۆججەت قىسقۇچنى قوشۇش مەغلۇپ بولدى، تاللايدىغان يول مەۋجۇت ئەمەس!</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="930"/>
        <source>Add search folder failed, permission denied!</source>
        <translation>ئىزدەش ھۆججەت قىسقۇچىنى قوشۇش مەغلۇپ بولدى، ئىجازەت رەت قىلىندى!</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="198"/>
        <location filename="../search.cpp" line="846"/>
        <location filename="../search.cpp" line="899"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="861"/>
        <location filename="../search.cpp" line="865"/>
        <location filename="../search.cpp" line="869"/>
        <location filename="../search.cpp" line="873"/>
        <location filename="../search.cpp" line="912"/>
        <location filename="../search.cpp" line="915"/>
        <location filename="../search.cpp" line="918"/>
        <location filename="../search.cpp" line="921"/>
        <location filename="../search.cpp" line="924"/>
        <location filename="../search.cpp" line="927"/>
        <location filename="../search.cpp" line="930"/>
        <source>Warning</source>
        <translation>دىققەت</translation>
    </message>
    <message>
        <source>Add blocked folder failed, choosen path is empty!</source>
        <translation type="vanished">چەكلەنگەن ھۆججەت قىسقۇچنى قوشۇش مەغلۇپ بولدى، تاللانغان يول بوش!</translation>
    </message>
    <message>
        <source>Add blocked folder failed, it is not in home path!</source>
        <translation type="vanished">چەكلەنگەن ھۆججەت قىسقۇچنى قوشۇش مەغلۇپ بولدى، ئۇ ئائىلە يولىدا ئەمەس!</translation>
    </message>
    <message>
        <source>Add blocked folder failed, its parent dir is exist!</source>
        <translation type="vanished">چەكلەنگەن ھۆججەت قىسقۇچنى قوشۇش مەغلۇپ بولدى، ئۇنىڭ ئانا دىدارى مەۋجۇت!</translation>
    </message>
    <message>
        <source>Add blocked folder failed, it has been already blocked!</source>
        <translation type="vanished">چەكلەنگەن ھۆججەت قىسقۇچنى قوشۇش مەغلۇپ بولدى، ئاللىقاچان چەكلەنگەن!</translation>
    </message>
</context>
</TS>
