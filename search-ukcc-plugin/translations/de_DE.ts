<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>Search</name>
    <message>
        <location filename="../search.cpp" line="41"/>
        <location filename="../search.cpp" line="279"/>
        <source>Search</source>
        <translation>Suchen</translation>
        <extra-contents_path>/Search/Search</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="296"/>
        <source>Create index</source>
        <translation>Index erstellen</translation>
        <extra-contents_path>/Search/Create index</extra-contents_path>
    </message>
    <message>
        <source>Creating index can help you getting results quickly.</source>
        <translation type="vanished">Das Erstellen eines Index kann Ihnen helfen, schnell Ergebnisse zu erhalten.</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="284"/>
        <source>Default web searching engine</source>
        <translation>Standard-Websuchmaschine</translation>
        <extra-contents_path>/Search/Default web searching engine</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="288"/>
        <source>baidu</source>
        <translation>Baidu</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="289"/>
        <source>sougou</source>
        <translation>Sougou</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="290"/>
        <source>360</source>
        <translation>360</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="443"/>
        <source>Create AI index</source>
        <translation>KI-Index erstellen</translation>
        <extra-contents_path>/Search/Create AI index</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="487"/>
        <source>Block Folders</source>
        <translation>Ordner blockieren</translation>
        <extra-contents_path>/Search/Block Folders</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="492"/>
        <source>Following folders will not be searched. You can set it by adding and removing folders.</source>
        <translation>Folgende Ordner werden nicht durchsucht. Sie können es festlegen, indem Sie Ordner hinzufügen und entfernen.</translation>
    </message>
    <message>
        <source>Choose folder</source>
        <translation type="vanished">添加</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="711"/>
        <location filename="../search.cpp" line="782"/>
        <source>delete</source>
        <translation>löschen</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="840"/>
        <location filename="../search.cpp" line="893"/>
        <source>Directories</source>
        <translation>Verzeichnisse</translation>
    </message>
    <message>
        <source>File Content Search</source>
        <translation type="vanished">Suche nach Dateiinhalten</translation>
        <extra-contents_path>/Search/File Content Search</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="382"/>
        <source>show more results that match the keyword</source>
        <translation>Weitere Ergebnisse anzeigen, die mit dem Keyword übereinstimmen</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="379"/>
        <source>Fuzzy Search</source>
        <translation>Fuzzy-Suche</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="196"/>
        <source>Create file content index will increase the usage of memory and CPU, do you still want to open it?</source>
        <translation>Der Index zum Erstellen von Dateiinhalten erhöht die Auslastung von Arbeitsspeicher und CPU, möchten Sie ihn trotzdem öffnen?</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="197"/>
        <source>OK</source>
        <translation>OKAY</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="309"/>
        <source>Create file index</source>
        <translation>Dateiindex erstellen</translation>
        <extra-contents_path>/Search/Create file index</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="334"/>
        <source>Create file content index</source>
        <translation>Erstellen eines Dateiinhaltsindex</translation>
        <extra-contents_path>/Search/Create file content index</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="366"/>
        <source>Precise Search</source>
        <translation>Präzise Suche</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="369"/>
        <source>show the results that exactly match the keyword</source>
        <translation>Zeigen Sie die Ergebnisse an, die genau mit dem Keyword übereinstimmen</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="415"/>
        <source>Index the text in pictures</source>
        <translation>Indizieren des Textes in Bildern</translation>
        <extra-contents_path>/Search/Use OCR to index the text in pictures</extra-contents_path>
    </message>
    <message>
        <source>Create ai index</source>
        <translation type="vanished">KI-Index erstellen</translation>
        <extra-contents_path>/Search/Create ai index</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="454"/>
        <source>Search Folders</source>
        <translation>Ordner durchsuchen</translation>
        <extra-contents_path>/Search/Search Folders</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="460"/>
        <source>Following folders will be searched. You can set it by adding and removing folders.</source>
        <translation>Folgende Ordner werden durchsucht. Sie können es festlegen, indem Sie Ordner hinzufügen und entfernen.</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="841"/>
        <source>select blocked folder</source>
        <translation>Blockierten Ordner auswählen</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="842"/>
        <location filename="../search.cpp" line="895"/>
        <source>Select</source>
        <translation>Auswählen</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="843"/>
        <location filename="../search.cpp" line="896"/>
        <source>Position: </source>
        <translation>Position: </translation>
    </message>
    <message>
        <location filename="../search.cpp" line="844"/>
        <location filename="../search.cpp" line="897"/>
        <source>FileName: </source>
        <translation>Dateiname: </translation>
    </message>
    <message>
        <location filename="../search.cpp" line="845"/>
        <location filename="../search.cpp" line="898"/>
        <source>FileType: </source>
        <translation>Dateityp: </translation>
    </message>
    <message>
        <location filename="../search.cpp" line="198"/>
        <location filename="../search.cpp" line="846"/>
        <location filename="../search.cpp" line="899"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="861"/>
        <location filename="../search.cpp" line="865"/>
        <location filename="../search.cpp" line="869"/>
        <location filename="../search.cpp" line="873"/>
        <location filename="../search.cpp" line="912"/>
        <location filename="../search.cpp" line="915"/>
        <location filename="../search.cpp" line="918"/>
        <location filename="../search.cpp" line="921"/>
        <location filename="../search.cpp" line="924"/>
        <location filename="../search.cpp" line="927"/>
        <location filename="../search.cpp" line="930"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="873"/>
        <location filename="../search.cpp" line="927"/>
        <source>Add search folder failed, hidden&#x3000;path is not supported!</source>
        <translation>Suchordner hinzufügen fehlgeschlagen, versteckter Pfad wird nicht unterstützt!</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="930"/>
        <source>Add search folder failed, permission denied!</source>
        <translation>Suchordner hinzufügen fehlgeschlagen, Berechtigung verweigert!</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="861"/>
        <source>Add blocked folder failed, its parent dir has been added!</source>
        <translation>Das Hinzufügen eines blockierten Ordners ist fehlgeschlagen, das übergeordnete Verzeichnis wurde hinzugefügt!</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="865"/>
        <source>Add blocked folder failed, choosen path is not exist!</source>
        <translation>Blockierten Ordner hinzufügen fehlgeschlagen, ausgewählter Pfad ist nicht vorhanden!</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="869"/>
        <source>Add blocked folder failed, it has already been blocked!</source>
        <translation>Blockierten Ordner hinzufügen fehlgeschlagen, er wurde bereits blockiert!</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="894"/>
        <source>select search folder</source>
        <translation>Suchordner auswählen</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="915"/>
        <source>Add search folder failed, choosen path is not supported currently!</source>
        <translation>Das Hinzufügen eines Suchordners ist fehlgeschlagen, der gewählte Pfad wird derzeit nicht unterstützt!</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="921"/>
        <source>Add search folder failed, another path which is in the same device has been added!</source>
        <translation>Suchordner hinzufügen fehlgeschlagen, ein weiterer Pfad, der sich auf demselben Gerät befindet, wurde hinzugefügt!</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="912"/>
        <source>Add search folder failed, choosen path or its parent dir has been added!</source>
        <translation>Suchordner hinzufügen fehlgeschlagen, ausgewählter Pfad oder übergeordnetes Verzeichnis wurde hinzugefügt!</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="918"/>
        <source>Add search folder failed, choosen path is in repeat mounted devices and another path which is in the same device has been added!</source>
        <translation>Das Hinzufügen eines Suchordners ist fehlgeschlagen, der gewählte Pfad befindet sich in wiederholt gemounteten Geräten und ein anderer Pfad, der sich auf demselben Gerät befindet, wurde hinzugefügt!</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="924"/>
        <source>Add search folder failed, choosen path is not exists!</source>
        <translation>Suchordner hinzufügen fehlgeschlagen, gewählter Pfad ist nicht vorhanden!</translation>
    </message>
</context>
</TS>
