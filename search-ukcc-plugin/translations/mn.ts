<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>Search</name>
    <message>
        <location filename="../search.cpp" line="41"/>
        <location filename="../search.cpp" line="279"/>
        <source>Search</source>
        <translation>ᠪᠦᠬᠦ᠌ ᠪᠠᠢᠳᠠᠯ᠎ᠤᠨ ᠬᠠᠢᠯᠳᠠ</translation>
        <extra-contents_path>/Search/Search</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="296"/>
        <source>Create index</source>
        <translation>ᠬᠡᠯᠬᠢᠶᠡᠰᠦ ᠪᠠᠢᠭᠤᠯᠬᠤ</translation>
        <extra-contents_path>/Search/Create index</extra-contents_path>
    </message>
    <message>
        <source>Creating index can help you getting results quickly.</source>
        <translation type="vanished">ᠡᠬᠢᠯᠡᠬᠦᠯᠦᠭᠰᠡᠨ᠎ᠦ ᠳᠠᠷᠠᠭ᠎ᠠ ᠬᠠᠢᠯᠲᠠ᠎ᠶᠢᠨ ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠩ᠎ᠢ ᠬᠤᠷᠳᠤᠨ ᠤᠯᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="284"/>
        <source>Default web searching engine</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠢᠨᠲᠸᠷᠨ᠋ᠸᠲ᠎ᠦᠨ ᠡᠷᠢᠯᠳᠡ ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ</translation>
        <extra-contents_path>/Search/Default web searching engine</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="288"/>
        <source>baidu</source>
        <translation>ᠪᠠᠢ ᠳ᠋ᠦ᠋</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="289"/>
        <source>sougou</source>
        <translation>ᠰᠸᠤ ᠭᠸᠦ</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="290"/>
        <source>360</source>
        <translation>360</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="309"/>
        <source>Create file index</source>
        <translation>ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠨᠡᠷ᠎ᠡ ᠶᠢᠨ ᠰᠦᠪᠡᠭᠴᠢ ᠶᠢ ᠡᠭᠦᠳᠦᠨ ᠪᠠᠶᠢᠭᠤᠯᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠶ᠃</translation>
        <extra-contents_path>/Search/Create file index</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="334"/>
        <source>Create file content index</source>
        <translation>ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠠᠭᠤᠯᠭ᠎ᠠ ᠶᠢᠨ ᠰᠦᠪᠡᠭᠴᠢ ᠶᠢ ᠡᠭᠦᠳᠦᠨ ᠪᠠᠶᠢᠭᠤᠯᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠶ᠃</translation>
        <extra-contents_path>/Search/Create file content index</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="487"/>
        <source>Block Folders</source>
        <translation>ᠢᠯᠭᠠᠵᠤ ᠭᠠᠷᠭᠠᠭᠰᠠᠨ ᠴᠤᠮᠤᠭ</translation>
        <extra-contents_path>/Search/Block Folders</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="492"/>
        <source>Following folders will not be searched. You can set it by adding and removing folders.</source>
        <translation>ᠬᠠᠢᠯᠲᠠ᠎ᠪᠠᠷ ᠳᠠᠷᠠᠭᠠᠬᠢ ᠴᠤᠮᠤᠭ᠎ᠢ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠦᠵᠡᠬᠦ᠌ ᠦᠬᠡᠢ ᠂ ᠨᠡᠮᠡᠬᠦ᠌ ᠪᠤᠶᠤ ᠬᠠᠰᠤᠬᠤ᠎ᠪᠠᠷ ᠳᠠᠮᠵᠢᠭᠤᠯᠤᠨ ᠢᠯᠭᠠᠵᠤ ᠭᠠᠷᠭᠠᠭᠰᠠᠨ ᠴᠤᠮᠤᠭ᠎ᠤᠨ ᠪᠠᠢᠷᠢ᠎ᠶᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Choose folder</source>
        <translation type="vanished">ᠨᠡᠮᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="711"/>
        <location filename="../search.cpp" line="782"/>
        <source>delete</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="840"/>
        <location filename="../search.cpp" line="893"/>
        <source>Directories</source>
        <translation>ᠴᠤᠮᠤᠭ</translation>
    </message>
    <message>
        <source>File Content Search</source>
        <translation type="vanished">ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠠᠭᠤᠯᠭ᠎ᠠ ᠡᠷᠢᠬᠦ</translation>
        <extra-contents_path>/Search/File Content Search</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="366"/>
        <source>Precise Search</source>
        <translation>ᠣᠨᠣᠪᠴᠢᠲᠠᠢ ᠡᠷᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="369"/>
        <source>show the results that exactly match the keyword</source>
        <translation>ᠵᠠᠩᠭᠢᠯᠠᠭ᠎ᠠ ᠶᠢᠨ ᠦᠰᠦᠭ ᠲᠡᠢ ᠪᠦᠷᠢᠮᠦᠰᠦᠨ ᠲᠣᠬᠢᠷᠠᠯᠴᠠᠭᠰᠠᠨ ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠩ ᠢ ᠢᠯᠡᠷᠡᠭᠦᠯᠵᠡᠢ</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="379"/>
        <source>Fuzzy Search</source>
        <translation>ᠪᠠᠯᠠᠷᠬᠠᠢ ᠡᠷᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="382"/>
        <source>show more results that match the keyword</source>
        <translation>ᠨᠡᠩ ᠠᠷᠪᠢᠨ ᠵᠠᠩᠭᠢᠯᠠᠭ᠎ᠠ ᠶᠢᠨ ᠦᠰᠦᠭ ᠲᠡᠢ ᠲᠣᠬᠢᠷᠠᠯᠴᠠᠭᠰᠠᠨ ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠩ ᠢ ᠢᠯᠡᠷᠡᠭᠦᠯᠵᠡᠢ</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="443"/>
        <source>Create AI index</source>
        <translation>AI ᠰᠦᠪᠡᠭᠴᠢ ᠶᠢ ᠡᠭᠦᠳᠦᠨ ᠪᠠᠶᠢᠭᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
        <extra-contents_path>/Search/Create AI index</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="454"/>
        <source>Search Folders</source>
        <translation>ᠡᠷᠢᠬᠦ ᠭᠠᠷᠴᠠᠭ</translation>
        <extra-contents_path>/Search/Search Folders</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="460"/>
        <source>Following folders will be searched. You can set it by adding and removing folders.</source>
        <translation>ᠳᠣᠣᠷᠠᠬᠢ ᠭᠠᠷᠴᠠᠭ ᠢ ᠡᠷᠢᠨ᠎ᠡ ᠃ ᠲᠠ ᠭᠠᠷᠴᠠᠭ ᠢᠶᠠᠷ ᠳᠠᠮᠵᠢᠨ ᠡᠷᠢᠬᠦ ᠬᠡᠪᠴᠢᠶ᠎ᠡ ᠶᠢ ᠵᠢᠭᠠᠨ ᠲᠣᠭᠲᠠᠭᠠᠵᠤ ᠪᠣᠯᠣᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="841"/>
        <source>select blocked folder</source>
        <translation>ᠢᠯᠭᠠᠵᠤ ᠭᠠᠷᠭᠠᠭᠰᠠᠨ ᠴᠤᠮᠤᠭ᠎ᠢ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="842"/>
        <location filename="../search.cpp" line="895"/>
        <source>Select</source>
        <translation>ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="843"/>
        <location filename="../search.cpp" line="896"/>
        <source>Position: </source>
        <translation>ᠪᠠᠢᠷᠢ ᠄ </translation>
    </message>
    <message>
        <location filename="../search.cpp" line="844"/>
        <location filename="../search.cpp" line="897"/>
        <source>FileName: </source>
        <translation>ᠹᠠᠢᠯ᠎ᠤᠨ ᠨᠡᠷ᠎ᠡ ᠄ </translation>
    </message>
    <message>
        <location filename="../search.cpp" line="845"/>
        <location filename="../search.cpp" line="898"/>
        <source>FileType: </source>
        <translation>ᠬᠡᠯᠪᠡᠷᠢ ᠮᠠᠶᠢᠭ </translation>
    </message>
    <message>
        <location filename="../search.cpp" line="198"/>
        <location filename="../search.cpp" line="846"/>
        <location filename="../search.cpp" line="899"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="196"/>
        <source>Create file content index will increase the usage of memory and CPU, do you still want to open it?</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠠᠭᠤᠯᠭ᠎ᠠ ᠶᠢ ᠡᠭᠦᠳᠦᠨ ᠪᠠᠶᠢᠭᠤᠯᠬᠤ ᠨᠢ ᠳᠣᠲᠣᠭᠠᠳᠤ ᠬᠠᠳᠠᠭᠠᠯᠠᠭᠤᠷ ᠪᠣᠯᠣᠨ CPU ᠶᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠯᠲᠡ ᠶᠢᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠶᠢ ᠨᠡᠮᠡᠭᠳᠡᠭᠦᠯᠦᠨ᠎ᠡ ᠂ ᠲᠠ ᠪᠠᠰᠠ ᠲᠡᠭᠦᠨ ᠢ ᠨᠡᠭᠡᠭᠡᠬᠦ ᠰᠠᠨᠠᠭ᠎ᠠ ᠲᠠᠢ ᠤᠤ ?</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="197"/>
        <source>OK</source>
        <translation>ᠡᠬᠢᠯᠡᠪᠡ᠃</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="415"/>
        <source>Index the text in pictures</source>
        <translation>ᠲᠠᠲᠠᠬᠤ ᠵᠢᠷᠤᠭ ᠳᠣᠲᠣᠷᠠᠬᠢ ᠦᠰᠦᠭ ᠪᠢᠴᠢᠭ᠌ ᠃</translation>
        <extra-contents_path>/Search/Use OCR to index the text in pictures</extra-contents_path>
    </message>
    <message>
        <source>Create ai index</source>
        <translation type="vanished">ai ᠰᠦᠪᠡᠭᠴᠢ ᠶᠢ ᠡᠭᠦᠳᠦᠨ ᠪᠠᠶᠢᠭᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
        <extra-contents_path>/Search/Create ai index</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="861"/>
        <location filename="../search.cpp" line="865"/>
        <location filename="../search.cpp" line="869"/>
        <location filename="../search.cpp" line="873"/>
        <location filename="../search.cpp" line="912"/>
        <location filename="../search.cpp" line="915"/>
        <location filename="../search.cpp" line="918"/>
        <location filename="../search.cpp" line="921"/>
        <location filename="../search.cpp" line="924"/>
        <location filename="../search.cpp" line="927"/>
        <location filename="../search.cpp" line="930"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="861"/>
        <source>Add blocked folder failed, its parent dir has been added!</source>
        <translation>ᠡᠨᠡ ᠭᠠᠷᠴᠠᠭ ᠢ ᠨᠡᠮᠡᠵᠦ ᠳᠡᠶᠢᠯᠬᠦ ᠦᠭᠡᠢ ᠂ ᠲᠡᠭᠦᠨ ᠦ ᠡᠴᠢᠭᠡ ᠶᠢᠨ ᠭᠠᠷᠴᠠᠭ ᠨᠢᠭᠡᠨᠲᠡ ᠨᠡᠮᠡᠭ᠍ᠳᠡᠵᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="865"/>
        <source>Add blocked folder failed, choosen path is not exist!</source>
        <translation>ᠭᠠᠷᠴᠠᠭ ᠨᠡᠮᠡᠵᠦ ᠳᠡᠶᠢᠯᠬᠦ ᠦᠭᠡᠢ ᠂ ᠡᠨᠡ ᠭᠠᠷᠴᠠᠭ ᠣᠷᠣᠰᠢᠬᠤ ᠦᠭᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="869"/>
        <source>Add blocked folder failed, it has already been blocked!</source>
        <translation>ᠭᠠᠷᠴᠠᠭ ᠨᠡᠮᠡᠵᠦ ᠳᠡᠶᠢᠯᠬᠦ ᠦᠭᠡᠢ ᠂ ᠡᠨᠡ ᠭᠠᠷᠴᠠᠭ ᠨᠢᠭᠡᠨᠲᠡ ᠨᠡᠮᠡᠭ᠍ᠳᠡᠵᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="873"/>
        <location filename="../search.cpp" line="927"/>
        <source>Add search folder failed, hidden&#x3000;path is not supported!</source>
        <translation>ᠭᠠᠷᠴᠠᠭ ᠨᠡᠮᠡᠬᠦ ᠶᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠭᠡᠢ ᠂ ᠨᠢᠭᠤᠴᠠ ᠭᠠᠷᠴᠠᠭ ᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠭᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="894"/>
        <source>select search folder</source>
        <translation>ᠡᠷᠢᠬᠦ ᠭᠠᠷᠴᠠᠭ ᠢ ᠰᠣᠩᠭᠣᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="912"/>
        <source>Add search folder failed, choosen path or its parent dir has been added!</source>
        <translation>ᠡᠷᠢᠬᠦ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ ᠨᠡᠮᠡᠵᠦ ᠢᠯᠠᠭᠳᠠᠵᠠᠢ ᠂ ᠨᠢᠭᠡᠨᠲᠡ ᠰᠤᠩᠭ᠋ᠤᠬᠤ ᠵᠠᠮ ᠪᠤᠶᠤ ᠲᠡᠭᠦᠨ ᠦ ᠡᠴᠢᠭᠡ ᠶᠢᠨ ᠭᠠᠷᠴᠠᠭ ᠨᠡᠮᠡᠵᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="915"/>
        <source>Add search folder failed, choosen path is not supported currently!</source>
        <translation>ᠡᠷᠢᠬᠦ ᠭᠠᠷᠴᠠᠭ ᠢ ᠨᠡᠮᠡᠵᠦ ᠢᠯᠠᠭᠳᠠᠵᠠᠢ ᠂ ᠣᠳᠣᠬᠠᠨ ᠳᠤ ᠡᠨᠡ ᠭᠠᠷᠴᠠᠭ ᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠭᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="918"/>
        <source>Add search folder failed, choosen path is in repeat mounted devices and another path which is in the same device has been added!</source>
        <translation>ᠡᠷᠢᠬᠦ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ ᠬᠠᠪᠴᠢᠭᠤᠯᠤᠨ ᠢᠯᠠᠭᠳᠠᠵᠠᠢ ᠂ ᠠᠷᠭ᠎ᠠ ᠵᠠᠮ ᠢ ᠰᠣᠩᠭᠣᠵᠤ ᠳᠠᠬᠢᠨ ᠳᠠᠪᠲᠠᠨ ᠠᠴᠢᠶᠠᠯᠠᠭᠰᠠᠨ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠳᠦ ᠨᠡᠮᠡᠵᠦ ᠂ ᠨᠢᠭᠡᠨᠲᠡ ᠠᠳᠠᠯᠢ ᠨᠢᠭᠡᠨ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠳᠣᠲᠣᠷᠠᠬᠢ ᠨᠥᠭᠥᠭᠡ ᠨᠢᠭᠡ ᠠᠷᠭ᠎ᠠ ᠵᠠᠮ ᠨᠡᠮᠡᠵᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="921"/>
        <source>Add search folder failed, another path which is in the same device has been added!</source>
        <translation>ᠡᠷᠢᠬᠦ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ ᠨᠡᠮᠡᠵᠦ ᠢᠯᠠᠭᠳᠠᠵᠠᠢ ᠂ ᠨᠢᠭᠡᠨᠲᠡ ᠠᠳᠠᠯᠢ ᠨᠢᠭᠡᠨ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠳᠣᠲᠣᠷᠠᠬᠢ ᠨᠥᠭᠥᠭᠡ ᠨᠢᠭᠡ ᠠᠷᠭ᠎ᠠ ᠵᠠᠮ ᠢ ᠨᠡᠮᠡᠵᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="924"/>
        <source>Add search folder failed, choosen path is not exists!</source>
        <translation>ᠨᠡᠮᠡᠵᠦ ᠡᠷᠢᠬᠦ ᠭᠠᠷᠴᠠᠭ ᠢᠯᠠᠭᠳᠠᠵᠠᠢ ᠂ ᠡᠨᠡ ᠭᠠᠷᠴᠠᠭ ᠣᠷᠣᠰᠢᠬᠤ ᠦᠭᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="930"/>
        <source>Add search folder failed, permission denied!</source>
        <translation>ᠡᠷᠢᠬᠦ ᠭᠠᠷᠴᠠᠭ ᠢ ᠨᠡᠮᠡᠵᠦ ᠢᠯᠠᠭᠳᠠᠵᠠᠢ ᠂ ᠡᠨᠡ ᠭᠠᠷᠴᠠᠭ ᠢ ᠰᠤᠷᠪᠤᠯᠵᠢᠯᠠᠬᠤ ᠡᠷᠬᠡ ᠦᠭᠡᠢ !</translation>
    </message>
    <message>
        <source>Add blocked folder failed, choosen path is empty!</source>
        <translation type="vanished">ᠨᠡᠮᠡᠯᠳᠡ ᠢᠯᠠᠭᠳᠠᠪᠠ ᠂ ᠰᠤᠩᠭᠤᠭᠰᠠᠨ ᠵᠠᠮ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <source>Add blocked folder failed, it is not in home path!</source>
        <translation type="vanished">ᠨᠡᠮᠡᠯᠳᠡ ᠢᠯᠠᠭᠳᠠᠪᠠ ᠂ ᠨᠡᠮᠡᠭᠰᠡᠨ ᠵᠠᠮ ᠲᠤᠰ ᠭᠠᠷᠴᠠᠭ᠎ᠤᠨ ᠳᠤᠤᠷ᠎ᠠ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <source>Add blocked folder failed, its parent dir is exist!</source>
        <translation type="vanished">ᠨᠡᠮᠡᠯᠳᠡ ᠢᠯᠠᠭᠳᠠᠪᠠ ᠂ ᠡᠬᠢ ᠭᠠᠷᠴᠠᠭ᠎ᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠨᠡᠮᠡᠭᠰᠡᠨ ᠪᠠᠢᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <source>Add blocked folder failed, it has been already blocked!</source>
        <translation type="vanished">ᠨᠡᠮᠡᠯᠳᠡ ᠢᠯᠠᠭᠳᠠᠪᠠ ᠂ ᠡᠨᠡ ᠴᠤᠮᠤᠭ᠎ᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠨᠡᠮᠡᠴᠢᠬᠡᠭᠰᠡᠨ ᠪᠠᠢᠨ᠎ᠠ!</translation>
    </message>
</context>
</TS>
